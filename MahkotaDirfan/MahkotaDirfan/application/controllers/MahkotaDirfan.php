<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MahkotaDirfan extends CI_Controller {

	public function MahkotaDirfan(){
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->model('User_model');
		$this->load->model('Category_model');
		$this->load->model('News_model');
		$this->load->model('Product_model');
		$this->load->model('Galery_model');
		$this->load->library('session');
	}

	public function index(){
		$data['Category'] = $this->Category_model->viewCategory();
		$data['News'] = $this->News_model->latestFiveNews();
		$data['Product'] = $this->Product_model->latestProduct();
		$data['Galery'] = $this->Galery_model->latestGalery();
		$this->load->view('Index.php', $data);
	}

	public function Login(){
		$data_session = array(
			'log'=>'true',
		);
		$this->session->set_userdata($data_session);
		redirect('Dashboard/view');
	}

	public function Logout(){
		$data['User']  = $this->User_model->GetUser();
		$this->load->view('Login', $data);
		$this->session->sess_destroy();
	}

	public function UserCheck(){
		$data['User']  = $this->User_model->GetUser();
		$this->load->view('Login',$data);
		$this->session->sess_destroy();
	}

	public function admin(){
		$this->session->sess_destroy();
		$data['User'] = $this->User_model->GetUser();
		$this->load->view('Login', $data);
	}
	
}
?>
