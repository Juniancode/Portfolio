<?php
class Product extends CI_Controller {

	public function Product(){
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->model('Product_model');
		$this->load->model('Category_model');
		$this->load->library('session');
	}

	public function index(){
		$this->session->sess_destroy();
		$data['Product'] = $this->Product_model->viewProduct();
		$data['Category'] = $this->Category_model->viewCategory();
		$data['name'] = '';
		$this->load->view('Product', $data);
	}

	public function Detail($id){
		$data['Product'] = $this->Product_model->getProduct($id);
		$this->load->view('Productdetail', $data);
	}

	function View(){
		if($this->session->userdata('log')){
			$data['Edit'] = false;
			$data['message'] ='';
			$data['Product'] = $this->Product_model->viewProduct();
			$data['Category'] = $this->Category_model->viewCategory();
			$this->load->view('admin/Product', $data);
		}else{
			redirect('MahkotaDirfan');
		}
	}

	function Category($category){
		$Name = $this->Category_model->getCategory($category);
		$catName ="";

		foreach ($Name as $name) {
			$catName = $name->name;
		}

		$data['Product'] = $this->Product_model->viewProductByCategory($catName);
		$data['Category'] = $this->Category_model->viewCategory();
		$data['name'] = $catName;
		$this->load->view('Product', $data);
	}

	public function Upload(){
		$config['upload_path']   = './pictures/product/';
        $config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']      = 4000;
		$config['remove_spaces'] = FALSE;

		$config['file_name'] = $this->input->post('name');

		$this->load->library('upload', $config);
		 if ( ! $this->upload->do_upload('file')) {
			$data['Edit'] = false;
			$data['message'] ='please select a picture';
			$data['Product'] = $this->Product_model->viewProduct();
			$data['Category'] = $this->Category_model->viewCategory();
			$this->load->view('admin/Product', $data);
		 }
		 else {
		 	$img_data=$this->upload->data();
            $new_imgname=$config['file_name'].$img_data['file_ext'];

			$productData = array(
				'name' => $this->input->post('name'),
				'note' => $this->input->post('note'),
				'category' => $this->input->post('category'),
				'picture' => $new_imgname
			);

			$this->Product_model->addProduct($productData);

		   redirect('Product/View');
		 }
	}

	public function deletePicture($id){
		if($this->session->userdata('log')){
			if(is_null($id)){
				redirect('/404/');
			}else{

				$Picture = $this->Product_model->getPicture($id);
				$name = '';
				foreach ($Picture as $picture) {
					$name = $picture->picture;
				}

				$this->load->helper('file');
				unlink('./pictures/product/'.$name);

				$this->Product_model->deletePicture($id);
				redirect('Product/View');
			}
		}else{
			redirect('MahkotaDirfan');
		}
	}

	public function updateProduct($id){
		if($this->session->userdata('log')){
			$data = array(
				'name' => $this->input->post('name'),
				'note' => $this->input->post('note'),
				'category' => $this->input->post('category'),
	        );
			$this->Product_model->updateProduct($data, $id);
			$this->View();
		}else{
			redirect('MahkotaDirfan');
		}
	}

	public function ViewUpdate($id){
		if($this->session->userdata('log')){
			$data['Product'] = $this->Product_model->viewProduct();
			$data['Edit'] = true;
			$data['id'] = $id;
			$data['Update'] = $this->Product_model->getPicture($id);
			$this->load->view('admin/Product', $data);
		}else{
			redirect('MahkotaDirfan');
		}
	}
}
?>