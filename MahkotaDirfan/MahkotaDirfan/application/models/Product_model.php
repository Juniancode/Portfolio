<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Product_model extends CI_Model {

		function __construct(){
			parent::__construct();
			$this->load->database();
		}

		function addProduct($productData){
			$this->db->insert('product',$productData);
		}

		function viewProduct(){
			$view = $this->db->get('product');
			return $view->result();
		}

		function viewProductByCategory($category){
			$view = $this->db->get_where('product',array('category'=>$category));
			return $view->result();
		}

		function deletePicture($id){
			$this->db->delete('product',array('id'=>$id));
		}

		public function getPicture($id){
			$Picture = $this->db->get_where('product',array('id' =>$id));
			return $Picture->result();
		}

		public function getProduct($id){
			$Picture = $this->db->get_where('product',array('id' =>$id));
			return $Picture->result();
		}

		public function countRow(){
			$count = $this->db->query('SELECT COUNT(id) AS COUNT FROM news');
			return $count->result();
		}

		public function latestProduct(){
			$view = $this->db->query('select * from product order by id DESC limit 5');
			return $view->result();
		}

		function updateProduct($data,$id){
			$this->db->where('id', $id);
			$this->db->update('product',$data);
		}
	}

?>