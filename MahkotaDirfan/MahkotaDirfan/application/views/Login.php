﻿<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Header  -->
    <?php $this->load->view('lib/Header'); ?>
    
    <title>Mahkota Dirfan - Login</title>

    <!-- Header Library -->
    <?php $this->load->view('lib/Headlib'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo base_url().'MahkotaDirfan/UserCheck' ?>" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input  type="text" class="form-control" placeholder="Username" id="username" name="username" type="text" autofocus autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input  type="password" class="form-control" placeholder="Password" id="password" name="password" type="text" 
                                </div>
                                <div class="checkbox">
                                    <!-- <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label> -->
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <a onclick="userCheck()" class="btn btn-lg btn-info btn-block">Login</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
        <!-- /# MODAL -->
    <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="userModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sign In</h4>
            </div>
            <div class="modal-body">
                <p>Wrond username and Password</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>
    </div>
    <!-- /#END MODAL -->

    <!-- Footer -->
    <?php $this->load->view('lib/Footer'); ?>

    <script>
    function userCheck(){
        <?php
            $check = 0;
            foreach($User as $user){?>
                if(("<?php echo $user->username; ?>" === document.getElementById('username').value) && ("<?php echo $user->password; ?>" === document.getElementById('password').value )){
                    window.location.href = "<?php echo base_url().'MahkotaDirfan/Login';?>";
                }else{
                    $('#userModal').modal('show');
                }
            <?php ;} ?>
    }
    </script>

</body>

</html>