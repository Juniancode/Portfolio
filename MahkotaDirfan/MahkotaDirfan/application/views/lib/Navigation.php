<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url().'Dashboard/View' ?>">Mahkota Dirfan</a>
            </div>
            <!-- /.navbar-header -->

            
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo base_url().'Dashboard/View' ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-gears fa-fw"></i> Manage<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'Category/View' ?>"><i class="fa fa-list-alt fa-fw"></i> Category</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url().'Product/View' ?>"><i class="fa fa-filter fa-fw"></i> Product</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'News/View' ?>"><i class="fa fa-book fa-fw"></i> News</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'Galery/View' ?>"><i class="fa fa-picture-o fa-fw"></i> Galery</a>
                        </li>
                        <li>
                            <a data-toggle="modal" href="#myModal"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>