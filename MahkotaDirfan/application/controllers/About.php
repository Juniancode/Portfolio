<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

	public function About(){
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->model('User_model');
		$this->load->library('session');
	}
	
	public function index(){
		$this->session->sess_destroy();
		$this->load->view('About');
	}
	
}
?>