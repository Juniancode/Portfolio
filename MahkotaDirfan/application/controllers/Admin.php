<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function Admin(){
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->model('User_model');
		$this->load->library('session');
	}
	
	public function index(){
		$this->session->sess_destroy();
		$data['User'] = $this->User_model->GetUser();
		$this->load->view('Login', $data);
	}
	
}
?>
