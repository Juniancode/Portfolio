<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function Dashboard(){
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->model('Galery_model');
		$this->load->model('News_model');
		$this->load->model('Category_model');
		$this->load->model('Product_model');
		$this->load->library('session');
	}

	public function View(){
		if ($this->session->userdata('log')) {
			$data['Galery'] = $this->Galery_model->countRow(); 
			$data['News'] = $this->News_model->countRow(); 
			$data['Product'] = $this->Product_model->countRow(); 
			$data['Category'] = $this->Category_model->countRow(); 
			$this->load->view('admin/Dashboard', $data);
		} else {
			redirect('MahkotaDirfan');
		}
	}
}
?>
