<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class News_model extends CI_Model {

		function __construct(){
			parent::__construct();
			$this->load->database();
		}

		function insertNews($newsData){
			$this->db->insert('news',$newsData);
		}

		function viewNews(){
			$view = $this->db->query('select * from news order by date DESC');
			return $view->result();
		}

		function latestNews(){
			$view = $this->db->query('select * from news order by date DESC limit 1');
			return $view->result();
		}

		function latestFiveNews(){
			$view = $this->db->query('select * from news order by date DESC limit 5');
			return $view->result();
		}

		function deleteNews($id){
			$this->db->delete('news',array('id'=>$id));
		}

		function getPicture($id){
			$name = $this->db->get_where('news',array('id'=>$id));
			return $name->result();
		}

		public function countRow(){
			$count = $this->db->query('SELECT COUNT(id) AS COUNT FROM news');
			return $count->result();
		}

		public function getNews($id){
			$name = $this->db->get_where('news',array('id'=>$id));
			return $name->result();
		}

		function updateNews($data,$id){
			$this->db->where('id', $id);
			$this->db->update('news',$data);
		}
	}

?>