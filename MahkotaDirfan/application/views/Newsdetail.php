<!doctype html>
<html class="no-js" lang="en">
  <head>
    <!-- Header  -->
    <?php $this->load->view('front/Headlib'); ?>
  </head>
  <body>
    <!-- Header  -->
    <?php $this->load->view('front/Header'); ?>

    <div class="sparatorhead">
      <div class="caption columns centered">
        <h1>LATEST <span>NEWS</span></h1>
        <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;/  News / Detail</p>
      </div>
    </div>
    <div class="container no-padding">
      <div class="row">
        <?php 
          foreach ($News as $news) {
                $originalDate = $news->date;
                $newDate = date("d-M-Y", strtotime($originalDate)); ?>
            <div class="heading">
              <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $news->title;?>
            </div>
            <div class="aboutus">
              <div class="large-12 columns no-padding">
                <div class="large-6 columns no-padding">
                  <h3> Publish on : <?php echo $newDate;?></h3>
                  <p><?php echo $news->note;?></p>
                </div>
                <div class="large-4 columns no-padding">
                  <img src="<?php echo base_url();?>pictures/news/<?php echo $news->picture?>" alt="">
                </div>
                <div class="large-2 columns no-padding">
                </div>
              </div>
            </div>
        <?php  }
        ?>
        
      </div>
    </div>
    <div class="medpartner no-padding">
      <center><img src="<?php echo base_url();?>assets/img/sponsored.jpg" class="centered" alt=""></center>
    </div>
    
    
    <?php $this->load->view('front/Footer'); ?>
    
    <?php $this->load->view('front/Footlib'); ?>
     
  </body>
</html>
