<!doctype html>
<html class="no-js" lang="en">
  <head>
    <!-- Header  -->
    <?php $this->load->view('front/Headlib'); ?>
  </head>
  <body>
    <!-- Header  -->
    <?php $this->load->view('front/Header'); ?>

    <div class="sparatorser">
      <div class="caption columns centered">
        <h1>OUR <span>PRODUCT</span></h1>
        <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;/  Our Product</p>
      </div>
    </div>
    <div class="container no-padding">
      <div class="row">
        <div class="heading">
          <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
          <?php
            $i =0;
            if($name == ''){
              foreach ($Category as $category) {
                if($i<1){
                  echo $category->name;
                  $cat = $category->name;
                }
                $i++;
              }
            }else{
                echo $name;
            } 
          ?>
        </div>
        <div class="product large-12 columns no-padding">
          <div class="item large-9 columns no-padding">
            <ul class="large-block-grid-4">
              <?php
                foreach ($Product as $product) {?>
                      <li>
                        <a href="<?php echo base_url().'Product/Detail/'.$product->id?>">
                        <h3><?php echo $product->name?></h3>
                        <img src="<?php echo base_url();?>pictures/product/<?php echo $product->picture?>" alt="">
                        </a>  
                      </li>
              <?php
                }
              ?>
            </ul>
          </div> 
          <div class="large-3 columns no-padding">
            <div class="titlecat">Product Categories</div>
            <ul class="category no-padding">
              <?php  
                foreach ($Category as $category) {?>
                  <li><a href="<?php echo base_url().'Product/Category/'.$category->id?>"><?php echo $category->name?></a></li>
              <?php  }
              ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="medpartner no-padding">
      <center><img src="<?php echo base_url();?>assets/img/sponsored.jpg" class="centered" alt=""></center>
    </div>
    
    
    <?php $this->load->view('front/Footer'); ?>
    
    <?php $this->load->view('front/Footlib'); ?>
     
  </body>
</html>
