<!doctype html>
<html class="no-js" lang="en">
  <head>
    <!-- Header  -->
    <?php $this->load->view('front/Headlib'); ?>
  </head>
  <body>
    <!-- Header  -->
    <?php $this->load->view('front/Header'); ?>

    <div class="sparatorser">
      <div class="caption columns centered">
        <h1>OUR <span>SERVICES</span></h1>
        <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;/  Our Services</p>
      </div>
    </div>
    <div class="container no-padding">
      <div class="row">
        <div class="heading">
          <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;Our Services
        </div>
        <div class="services large-12 columns no-padding">
          <div class="large-6 columns no-padding">
            <img src="<?php echo base_url();?>assets/img/content/marketing.jpg" alt="">
          </div>
          <div class="large-6 columns no-padding">
            <h1>Marketing & Sales Services</h1>
            <p>We have more than 150 sales and marketing staffs in our company. Each business division has their own marketing team, supported by highly trained & qualified product and application specialists.</p>
          </div>
        </div>
        <div class="aftersale large-12 columns no-padding">
          <div class="large-9 columns no-padding">
           <h1>After Sales Services</h1>
            <p>We believe in exellence services where we maintain prolonged customer satisfaction through our services. As such, PT. Mahkota Dirfan Berkah has a subsidiary focusing on the Satisfy guarantee, service and fast respons.</p>
          </div>
          <div class="large-3 columns no-padding">
            <img src="<?php echo base_url();?>assets/img/content/afterservices.jpg" alt="">
          </div>
        </div>
        <div class="services large-12 columns no-padding">
          <div class="large-6 columns no-padding">
            <img src="<?php echo base_url();?>assets/img/content/logistik.jpg" alt="">
          </div>
          <div class="large-6 columns no-padding">
            <h1>Marketing & Sales Services</h1>
            <p>We have more than 150 sales and marketing staffs in our company. Each business division has their own marketing team, supported by highly trained & qualified product and application specialists.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="medpartner no-padding">
      <center><img src="<?php echo base_url();?>assets/img/sponsored.jpg" class="centered" alt=""></center>
    </div>
    
    <?php $this->load->view('front/Footer'); ?>
    
    <?php $this->load->view('front/Footlib'); ?>
     
  </body>
</html>
