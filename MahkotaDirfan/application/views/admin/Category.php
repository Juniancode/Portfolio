<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Header  -->
    <?php $this->load->view('lib/Header'); ?>
    
    <title>Mahkota Dirfan - Category</title>

    <!-- Header Library -->
    <?php $this->load->view('lib/Headlib'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php $this->load->view('lib/Navigation'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Category</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">
                <?php
                    if ($Edit == false) { ?>
                        <form role="form" method="post" action="<?php echo base_url().'Category/submitCategory' ?>" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Form Submit Category
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                             <div class="form-group">
                                                <label>Type Category Here</label>
                                                <input class="form-control" name="type" autocomplete="off" required>
                                                <p class="help-block"><!-- Example block-level help text here. --></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label>Upload Picture</label>
                                            <input type="file" class="form-control" name="file" >
                                                <p class="help-block" style="font-size: 18px; color: red; text-align: center;">
                                                    <?php 
                                                        echo $message;
                                                    ?>
                                                </p>
                                        </div>
                                    </div>
                                    </br>
                                    <div class="row">
                                        <div class="col-lg-12" style="text-align:center">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                            <button type="reset" class="btn btn-danger">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                <?php    }
                    else{   ?>
                            <form role="form" method="post" action="<?php echo base_url().'Category/updateCategory/'.$id ?>" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Form Update Category
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                 <div class="form-group">
                                                    <label>Type Category Here</label>
                                                    <?php
                                                        foreach ($Update as $update) { ?>
                                                            <input class="form-control" name="type" autocomplete="off" value="<?php echo $update->name; ?>">
                                                    <?php    }
                                                    ?>
                                                    <p class="help-block"><!-- Example block-level help text here. --></p>
                                                </div>
                                            </div>
                                        </div>
                                        </br>
                                        <div class="row">
                                            <div class="col-lg-12" style="text-align:center">
                                                <button type="submit" class="btn btn-success">Update</button>
                                                <button type="reset" class="btn btn-danger">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                <?php    }
                ?>
                
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

                        </br>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Category List 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Category</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $i = 1;
                                        foreach ($Category as $category) { 
                                            if ($i % 2 == 0 ) { ?>
                                                <tr class="odd gradeX">
                                            <?php }else{ ?>
                                                <tr class="even gradeC">
                                            <?php } ?>
                                                <td><?php echo $category->id ?></td>
                                                <td>
                                                    <?php echo $category->name ?>
                                                </td>
                                                <td class="center">
                                                    <center>
                                                        <a>
                                                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#<?php echo $category->id; ?>"><span class="fa fa-trash"></span>
                                                        </button>
                                                        </a>
                                                        <a href="<?php echo base_url().'Category/ViewUpdate/'.$category->id ?>">
                                                         <button type="button" class="btn btn-xs btn-warning"><span class="fa fa-edit"></span>
                                                        </button>
                                                        </a>
                                                    </center>
                                                </td>
                                            </tr>

                                            <div class="modal fade" id="<?php echo $category->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <h3>Are you sure? </h3>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                    <a> <?php echo anchor('/Category/deleteCategory/'.$category->id,'<button type="button" class="btn btn-danger">Delete</button>') ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                    <?php       
                                       $i++; }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /#wrapper -->

    <!-- Footer -->
    <?php $this->load->view('lib/Footer'); ?>
    
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
    

</body>

</html>
