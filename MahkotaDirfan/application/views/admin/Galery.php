<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Header  -->
    <?php $this->load->view('lib/Header'); ?>
    
    <title>Mahkota Dirfan - Galery</title>

    <!-- Header Library -->
    <?php $this->load->view('lib/Headlib'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php $this->load->view('lib/Navigation'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Galery</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">
                                <?php 
                                        if($Edit == false){?>
                <form role="form" method="post" action="<?php echo base_url().'Galery/Upload' ?>" enctype="multipart/form-data">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form upload picture
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Caption</label>
                                                <textarea class="form-control" rows="3" name="pictureCaption" required></textarea>
                                                <p class="help-block"><!-- Example block-level help text here. --></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-8">
                                                <label>Browse Picture</label>
                                                <input type="file" name="file" id="version" class="form-control" >
                                                <p class="help-block" style="font-size: 18px; color: red; text-align: center;">
                                                    <?php 
                                                        echo $message;
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                            <div class="row" style="text-align: center;">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-success">Upload</button>
                                    <button type="reset" class="btn btn-danger">Cancel</button>
                                </div>
                            </div>
                                    <?php    }
                                        else{ 
                                            foreach ($Update as $update) { ?>
                                                <form role="form" method="post" action="<?php echo base_url().'Galery/updateGalery/'.$id ?>" enctype="multipart/form-data">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form update picture
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Caption</label>
                                                <textarea required class="form-control" rows="3" name="pictureCaption"><?php echo $update->caption; ?></textarea>
                                                <p class="help-block"><!-- Example block-level help text here. --></p>
                                            </div>
                                        </div>
                                    </div>
                            <div class="row" style="text-align: center;">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-success">Update</button>
                                    <button type="reset" class="btn btn-danger">Cancel</button>
                                </div>
                            </div>
                                    <?php   }
                                        }
                                ?>
                        </div>
                    </div>
                    </form>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

                        </br>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Picture List 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Caption</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $i = 1;
                                        foreach ($Galery as $galery) { 
                                            if ($i % 2 == 0 ) { ?>
                                                <tr class="odd gradeX">
                                            <?php }else{ ?>
                                                <tr class="even gradeC">
                                            <?php } ?>
                                                <td><?php echo $galery->id ?></td>
                                                <td>
                                                <a target="_blank" href="<?php echo base_url();?>pictures/galery/<?php echo $galery->picture?>">
                                                    <?php echo $galery->caption ?>
                                                </a>
                                                </td>
                                                <td class="center">
                                                    <center>
                                                        <a>
                                                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#<?php echo $galery->id; ?>"><span class="fa fa-trash"></span>
                                                        </button>
                                                        </a>
                                                        <a href="<?php echo base_url().'Galery/ViewUpdate/'.$galery->id ?>">
                                                         <button type="button" class="btn btn-xs btn-warning"><span class="fa fa-edit"></span>
                                                        </button>
                                                        </a>
                                                    </center>
                                                </td>
                                            </tr>

                                            <div class="modal fade" id="<?php echo $galery->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <h3>Are you sure? </h3>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                    <a> <?php echo anchor('/Galery/deletePicture/'.$galery->id,'<button type="button" class="btn btn-danger">Delete</button>') ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                    <?php       
                                       $i++; }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
    
    <!-- Footer -->
    <?php $this->load->view('lib/Footer'); ?>
    
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
    

</body>

</html>
