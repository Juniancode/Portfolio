<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Header  -->
    <?php $this->load->view('lib/Header'); ?>
    
    <title>Mahkota Dirfan - Product</title>

    <!-- Header Library -->
    <?php $this->load->view('lib/Headlib'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php $this->load->view('lib/Navigation'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Product</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">
                <?php
                    if ($Edit == false) { ?>
                        <form role="form" method="post" action="<?php echo base_url().'Product/Upload' ?>" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Form Insert Product
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                             <div class="form-group">
                                                <label>Product Name</label>
                                                <input class="form-control" name="name" autocomplete="off" required>
                                                <p class="help-block"><!-- Example block-level help text here. --></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Note</label>
                                                <textarea class="form-control" rows="8" name="note" required></textarea>
                                                <p class="help-block"><!-- Example block-level help text here. --></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-8">
                                                <label>Category</label>
                                                <select required name="category" id="category" class="form-control select2" style="width: 100%;">
                                                        <option value="">Select Category</option>
                                                        <?php foreach($Category as $category){
                                                            ?>
                                                                <option value="<?php echo $category->name; ?>"> 
                                                                    <?php echo $category->name; ?>
                                                                </option>
                                                        <?php }   ?>
                                                    </select>
                                            </div>
                                            <div class="form-group col-lg-8">
                                                <label>Upload Picture</label>
                                                <input type="file" class="form-control" name="file">
                                                <p class="help-block" style="font-size: 18px; color: red; text-align: center;">
                                                    <?php 
                                                        echo $message;
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="text-align: center;">
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn btn-success">Insert</button>
                                            <button type="reset" class="btn btn-danger">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                <?php    }
                    else{ 
                        foreach ($Update as $update) { ?>
                            <form role="form" method="post" action="<?php echo base_url().'Product/updateProduct/'.$id ?>" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Form Update Product
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                             <div class="form-group">
                                                <label>Product Name</label>
                                                <input class="form-control" name="name" autocomplete="off" value="<?php echo $update->name;?>">
                                                <p class="help-block"><!-- Example block-level help text here. --></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Note</label>
                                                <textarea class="form-control" rows="8" name="note"><?php echo $update->note;?></textarea>
                                                <p class="help-block"><!-- Example block-level help text here. --></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-8">
                                                <label>Category</label>
                                                <select required name="category" id="category" class="form-control select2" style="width: 100%;">
                                                        <option value="<?php echo $update->name;?>"><?php echo $update->name;?></option>
                                                        <?php foreach($Category as $category){
                                                            ?>
                                                                <option value="<?php echo $category->name; ?>"> 
                                                                    <?php echo $category->name; ?>
                                                                </option>
                                                        <?php }   ?>
                                                    </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="text-align: center;">
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn btn-success">Update</button>
                                            <button type="reset" class="btn btn-danger">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                <?php    }
                    }
                ?>
                
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

                        </br>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Product List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Note</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $i = 1;
                                        foreach ($Product as $product) { 
                                            if ($i % 2 == 0 ) { ?>
                                                <tr class="odd gradeX">
                                            <?php }else{ ?>
                                                <tr class="even gradeC">
                                            <?php } ?>
                                                <td><?php echo $product->id ?></td>
                                                <td>
                                                <a target="_blank" href="<?php echo base_url();?>pictures/product/<?php echo $product->picture?>">
                                                    <?php echo $product->name ?>
                                                </a>
                                                </td>
                                                <td><?php echo $product->category ?></td>
                                                <td><?php echo $product->note ?></td>
                                                <td class="center">
                                                    <center>
                                                        <a>
                                                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#<?php echo $product->id; ?>"><span class="fa fa-trash"></span>
                                                        </button>
                                                        </a>
                                                        <a href="<?php echo base_url().'Product/ViewUpdate/'.$product->id ?>">
                                                         <button type="button" class="btn btn-xs btn-warning"><span class="fa fa-edit"></span>
                                                        </button>
                                                        </a>
                                                    </center>
                                                </td>
                                            </tr>

                                            <div class="modal fade" id="<?php echo $product->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <h3>Are you sure? </h3>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                    <a> <?php echo anchor('/Product/deletePicture/'.$product->id,'<button type="button" class="btn btn-danger">Delete</button>') ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                    <?php       
                                       $i++; }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /#wrapper -->
    
    

    <!-- Footer -->
    <?php $this->load->view('lib/Footer'); ?>
    
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
    

</body>

</html>
