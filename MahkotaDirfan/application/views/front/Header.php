<div class="header ">
        <div class="row top">
          <div class="left">
            <ul>
              <li><a href="<?php echo base_url().'MahkotaDirfan' ?>">Home</a></li>
              <li><a href="<?php echo base_url().'About' ?>">About</a></li>
              <li><a href="<?php echo base_url().'News' ?>">News</a></li>
              <li><a href=""><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>&nbsp;&nbsp;Printable Catalog</a></li>
            </ul>
          </div>
          <div class="right">
            <ul>
              <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;(021) 8835 6330</a></li>
              <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;mahkotadirfan@yahoo.co.id</a></li>
            </ul>
          </div>
           <div id="polyglotLanguageSwitcher">
            <form action="#">
              <select id="polyglot-language-options">
                <option id="id" value="id" selected></option>
                <option id="en" value="en"></option>
              </select>
            </form>
          </div>
        </div>
    </div>
    <div class="mainheader">
      <div class="row maintop">
        <div class="logo">
          <a href="<?php echo base_url().'MahkotaDirfan' ?>"><img src="<?php echo base_url();?>assets/img/logo.png" alt=""></a>
        </div>
        <div class="menu">
          <ul class="no-list no-padding">
            <li><a href="<?php echo base_url().'Product' ?>">PRODUCT</a></li>
            <li><a href="<?php echo base_url().'Services' ?>">SERVICES</a></li>
            <li><a href="#">CONTACT US</a></li>
            <li><a href="<?php echo base_url().'Galery' ?>">GALERY</a></li>
          </ul>
        </div>
      </div>
    </div>
    