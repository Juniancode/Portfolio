    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Mahkota Dirfan Berkah</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/app.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/mahkotadirfan.css">
    <link href="<?php echo base_url();?>assets/css/polyglot-language-switcher.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link href='https://fonts.googleapis.com/css?family=Lato:400,900,700,300,100' rel='stylesheet' type='text/css'>
    <script src="<?php echo base_url();?>assets/bower_components/modernizr/modernizr.js"></script>